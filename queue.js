let collection = [];

// Write the queue functions below.
// 1. Output all the elements of the queue

    let print = () => {
      return collection;
    };

// 2. Adds element to the rear of the queue

    let enqueue = (element) => {
	
	    collection.push(element);

	    return collection;
    };

// 3. Removes element from the front of the queue
  
    let dequeue = (element) => {
	    collection.shift(element);
	    return collection;
    };

// 4. Show element at the front
    let front = () => {
	    return collection[0];
    };

// 5. Show the total number of elements

    let size = () => {
	    return collection.length;
    };

// 6. Outputs a Boolean value describing whether queue is empty or not

    let isEmpty = () => {
	    return collection.length === 0;
    };

// 7. Export your functions
module.exports = {
  print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty,
};